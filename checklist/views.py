from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.contrib.auth.models import User
from .models import CheckList, Point, Field
from status.models import Status
from scheadule.models import Check
from signature.models import CheckListSign
from .render import Render
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail, EmailMultiAlternatives
from signature.models import ReportSign
from report.models import Report, ReportPoint
from scheadule.models import Check, Check_status

def index(request):    
    if request.user.id:
        data = {'user': User.objects.get(id=request.user.id)}
        if request.user.groups.filter(name='Supervisor').exists():
            return redirect('/checklists')
        elif request.user.groups.filter(name='Manager').exists() or request.user.is_superuser  or request.user.is_staff:
            return redirect('/reports')
        
        return render(request, '_main/main.html', data)
    
    return render(request, 'registration/login.html')

@login_required
def checklists(request):
    checklists = CheckList.objects.order_by('sys_number').filter(show=True)
    data = {'checklists': checklists}
    
    return render(request, 'checklists/checklists.html', data)

@login_required
def checklist(request, checklist_id):
    checklist = CheckList.objects.filter(id=checklist_id)
    points = Point.objects.filter(check_list__id=checklist_id).order_by('sub_check_list', 'header_position', 'position')
    header_field = Field.objects.filter(check_list__id=checklist_id)
    signs = CheckListSign.objects.filter(check_list__id=checklist_id).values()
    data = {'points':points, 'checklist':checklist, 'signs':signs, 'header_field':header_field}
    
    return render(request, 'checklists/checklist.html', data)

@login_required
def add_checklist(request, checklist_id):
    checklist = CheckList.objects.filter(id=checklist_id)
    points = Point.objects.filter(check_list__id=checklist_id).values()
    signs = CheckListSign.objects.filter(check_list__id=checklist_id).values()
    data = {'points':points, 'checklist':checklist, 'signs':signs}
    
    return render(request, 'checklists/checklist.html', data)

@login_required
def checklist_pdf(request, checklist_id):
    checklist = CheckList.objects.filter(id=checklist_id)
    points = Point.objects.filter(check_list__id=checklist_id).values()
    data = {'points':points, 'checklist':checklist}
    
    return Render.render('checklists/checklist_pdf.html', data)

def send_report_mail(request, report_id):
    data = {}
    report = Report.objects.filter(id=report_id)
    report_file_name = report[0].cluster_number+'_'+report[0].well_number+'_'+report[0].check_list.name+'_'+report[0].supervisor.first_name+'_'+report[0].supervisor.last_name
    
    supervisor = report[0].supervisor.first_name+' '+report[0].supervisor.last_name
    checklist_name = report[0].check_list.name
    well_number = report[0].well_number
    
    report = {}

    if request.user.groups.filter(name='Supervisor').exists():
        report = Report.objects.filter(id=report_id).filter(supervisor__id=request.user.id)
    elif request.user.groups.filter(name='Manager').exists() or request.user.is_superuser  or request.user.is_staff:
        report = Report.objects.filter(id=report_id)
        
    if report != {}:
        report_r = Report.objects.get(id=report[0].id)
        report_r.status = Status.objects.get(name='done')
        report_r.save()
        
        report_points = ReportPoint.objects.filter(report__id=report_id).order_by('point__sub_check_list', 'point__header_position', 'point__position')
        report_signs = ReportSign.objects.filter(report__id=report_id).order_by('id')
        data = {'report_points':report_points, 'report_signs':report_signs, 'report':report}
    
    Render.create_file('reports/report_pdf.html', data, report_id)

    report_recipients = list(User.objects.filter(groups__name='ReportRecipients').values('email'))
    report_recipients_emails = [i['email'] for i in report_recipients]
    
    subject = 'Отчет по скважине '+well_number+' супервайзера '+supervisor+': '+checklist_name
    text_content = """
        Отчет супервайзера можно посмотреть в прикрепленном pdf файле
        Скважина: """+well_number+"""
        Чеклист: """+checklist_name+"""
        Супервайзер: """+supervisor+"""
    """
    msg = EmailMultiAlternatives(subject, text_content, 'report@pnsupervisor.com', report_recipients_emails)
    msg.attach_file('static/pdf/'+report_file_name+'.pdf')
    msg.send()
    
    try:
        check = Check.objects.get(report__id=report_id)
        check.status = Check_status.objects.get(name='done')
        check.save()
    except Check.DoesNotExist:
        check = None

    return HttpResponse(request)