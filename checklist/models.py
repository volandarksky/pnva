from django.db import models

class CheckList(models.Model):
    name = models.CharField(max_length=250, verbose_name='название')
    sys_number = models.IntegerField(blank=True, null=False, default=None, verbose_name='системный номер')
    show = models.BooleanField(blank=True, default=True, verbose_name='показывать')
    created = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name='создан')
    
    def __str__(self):
        return("%s" % (self.name))
    
    class Meta:
        verbose_name = 'Чеклист'
        verbose_name_plural = 'Чеклисты'

class Point(models.Model):
    sub_check_list = models.IntegerField(blank=True, null=True, default=1, verbose_name='список')
    header_position = models.IntegerField(blank=True, null=True, default=None, verbose_name='позиция')
    position = models.IntegerField(blank=True, null=True, default=None, verbose_name='подпозиция')
    
    text = models.TextField(verbose_name='текст')
    check_list = models.ForeignKey(CheckList, blank=True, null=True, default=None, on_delete=models.SET_NULL)
    created = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name='создан')
    
    class Meta:
        verbose_name = 'Пункт'
        verbose_name_plural = 'Пункты'
        
class Field(models.Model):
    
    html_name = models.CharField(max_length=250, verbose_name='название')
    html_id = models.CharField(max_length=250)
    html_type = models.CharField(max_length=250, verbose_name='тип', default='text')
    html_placeholder = models.CharField(max_length=250, default='заполнить')

    check_list = models.ForeignKey(CheckList, blank=True, null=True, default=None, on_delete=models.SET_NULL)
    created = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name='создан')
    
    class Meta:
        verbose_name = 'Поле в шапке'
        verbose_name_plural = 'Поля в шапке'
