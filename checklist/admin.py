from django.contrib import admin
from django.forms import TextInput, Textarea 

from .models import *
from signature.models import CheckListSign

class PointAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Point._meta.fields]
    list_filter = ['check_list']
    class Meta:
        model = Point    

class FieldAdmin(admin.ModelAdmin):
    # list_display = [Field.html_name for field in Field._meta.fields]
    list_filter = ['check_list']
    class Meta:
        model = Field   

class PointInline(admin.TabularInline):
    model = Point
    formfield_overrides = {
        models.TextField: {'widget': Textarea(attrs={'rows':2, 'cols':100})},
    }
    extra = 0
    ordering = ('sub_check_list', 'header_position', 'position')
    
class SignInline(admin.TabularInline):
    model = CheckListSign
    extra = 0
    
class FieldInline(admin.TabularInline):
    model = Field
    extra = 0

class CheckListAdmin(admin.ModelAdmin):
    list_display = ['sys_number', 'name', 'show']
    list_display_links = ['name']
    ordering = ('sys_number',)
    
    inlines = (PointInline, SignInline, FieldInline)
    
    class Meta:
        model = CheckList   
    

# admin.site.register(Point, PointAdmin)         
admin.site.register(Field, FieldAdmin)         
admin.site.register(CheckList, CheckListAdmin)