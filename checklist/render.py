from io import BytesIO, StringIO
from django.http import HttpResponse
from django.template.loader import get_template
import xhtml2pdf.pisa as pisa
from django.core.mail import EmailMessage
from report.models import Report

import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def transliterate(word):

   literals = {'а':'a','б':'b','в':'v','г':'g','д':'d','е':'e','ё':'e',
      'ж':'zh','з':'z','и':'i','й':'i','к':'k','л':'l','м':'m','н':'n',
      'о':'o','п':'p','р':'r','с':'s','т':'t','у':'u','ф':'f','х':'h',
      'ц':'c','ч':'cz','ш':'sh','щ':'scz','ъ':'','ы':'y','ь':'','э':'e',
      'ю':'u','я':'ja', 'А':'A','Б':'B','В':'V','Г':'G','Д':'D','Е':'E','Ё':'E',
      'Ж':'ZH','З':'Z','И':'I','Й':'I','К':'K','Л':'L','М':'M','Н':'N',
      'О':'O','П':'P','Р':'R','С':'S','Т':'T','У':'U','Ф':'F','Х':'H',
      'Ц':'C','Ч':'CZ','Ш':'SH','Щ':'SCH','Ъ':'','Ы':'y','Ь':'','Э':'E',
      'Ю':'U','Я':'YA',',':'','?':'',' ':'_','~':'','!':'','@':'','#':'',
      '$':'','%':'','^':'','&':'','*':'','(':'',')':'','-':'','=':'','+':'',
      ':':'',';':'','<':'','>':'','\'':'','"':'','\\':'','/':'','№':'',
      '[':'',']':'','{':'','}':'','ґ':'','ї':'', 'є':'','Ґ':'g','Ї':'i',
      'Є':'e', '—':''}

   for key in literals:
      word = word.replace(key, literals[key])
      
   return word


class Render:

    @staticmethod
    def render(path: str, params: dict, report_id: int):
        template = get_template(path)
        html = template.render(params)
        
        response = BytesIO()
        pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-16")), response)
        
        file = open(os.path.join(BASE_DIR, "static")+'/pdf/report_'+report_id+'.pdf', "w+b")
        pisaStatus = pisa.CreatePDF(html.encode('UTF-16'), dest=file, encoding='utf-8')
        
        if not pdf.err:
            return HttpResponse(response.getvalue(), content_type='application/pdf')
        else:
            return HttpResponse("Error Rendering PDF", status=400)
        
    @staticmethod
    def create_file(path: str, params: dict, report_id: int):
        template = get_template(path)
        html = template.render(params)
        
        report = Report.objects.filter(id=report_id)
        report_file_name = report[0].cluster_number+'_'+report[0].well_number+'_'+report[0].check_list.name+'_'+report[0].supervisor.first_name+'_'+report[0].supervisor.last_name
        # report_file_name = transliterate(report_file_name)
        print(report_file_name)
        file = open(os.path.join(BASE_DIR, "static")+'/pdf/'+report_file_name+'.pdf', "w+b")
        # file = open(os.path.join(BASE_DIR, "static")+'/pdf/report_'+report_id+'.pdf', "w+b")
        pisaStatus = pisa.CreatePDF(html.encode('UTF-16'), dest=file, encoding='utf-8')
