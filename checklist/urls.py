from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from . import views
from django.conf import settings
from django.conf.urls.static import static

admin.autodiscover()

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    url(r'^$', views.index, name='index'),
    url(r'^checklists/$', views.checklists, name='checklists'),
    url(r'^send_report_mail/(?P<report_id>[0-9]+)/$', views.send_report_mail, name='send_report_mail'),
    url(r'^checklists/(?P<checklist_id>[0-9]+)/$', views.checklist, name='checklist'),
    url(r'^checklists/(?P<checklist_id>[0-9]+)/$', views.add_checklist, name='add_checklist'),
    url(r'^checklists/(?P<checklist_id>[0-9]+)/pdf/$', views.checklist_pdf, name='checklist_pdf'),
    url(r'^', include('report.urls')),
    url(r'^', include('scheadule.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
