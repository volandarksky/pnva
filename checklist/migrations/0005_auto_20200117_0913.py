# Generated by Django 2.2.6 on 2020-01-17 09:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('checklist', '0004_point_sub_check_list'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='checklist',
            options={'verbose_name': 'Чеклист', 'verbose_name_plural': 'Чеклисты'},
        ),
        migrations.AddField(
            model_name='checklist',
            name='hidden',
            field=models.BooleanField(blank=True, default=False, verbose_name='скрыт'),
        ),
    ]
