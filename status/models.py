from django.db import models

class Status(models.Model):
    name = models.CharField(max_length=250)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    
    def __str__(self):
        return("%s" % (self.name))