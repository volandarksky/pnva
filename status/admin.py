from django.contrib import admin 
from .models import *

class StatusAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Status._meta.fields]
    list_filter = ['name']
    class Meta:
        model = Status
        
admin.site.register(Status, StatusAdmin)