from django.db import models
from checklist.models import CheckList
from django.contrib.auth.models import User
from report.models import Report

def get_first_name(self):
    return ("%s %s %s" % (self.first_name, self.last_name, self.username))

User.add_to_class("__str__", get_first_name)

class Check_status(models.Model):
    name = models.CharField(max_length=250)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    
    def __str__(self):
        return("%s" % (self.name))
    
class Check(models.Model):
    check_list = models.ForeignKey(CheckList, blank=False, null=True, default=None, on_delete=models.SET_NULL, verbose_name='чеклист')
    report = models.ForeignKey(Report, blank=True, null=True, default=None, on_delete=models.SET_NULL, verbose_name='отчет')
    supervisor = models.ForeignKey(User, blank=False, null=True, default=None, on_delete=models.SET_NULL, limit_choices_to={'groups__name': 'Supervisor'}, verbose_name='супервизор')
    date = models.DateTimeField(auto_now=False, verbose_name='дата')
    status = models.ForeignKey(Check_status, blank=True, null=True, default=Check_status.objects.get(id=1), on_delete=models.SET_NULL, verbose_name='статус')
    created = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name='создан')
    
    def __str__(self):
        return("%s" % (self.check_list))
    
    class Meta:
        verbose_name = 'Проверка'
        verbose_name_plural = 'Проверки'