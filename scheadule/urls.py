from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from . import views

admin.autodiscover()

urlpatterns = [
    url(r'^scheadule/$', views.scheadule, name='scheadule'),
]
