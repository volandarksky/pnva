from django.shortcuts import render
from django.http import HttpResponse
from django.template import RequestContext, loader
from django.contrib.auth.decorators import login_required
from .models import Check
from status.models import Status
from datetime import *
from time import *
import json

@login_required
def scheadule(request):
    
    if request.user.groups.filter(name='Supervisor').exists():
        
        checks = Check.objects.filter(supervisor__id=request.user.id)
        checks_as_list = []
        
        for check in checks:
            
            timedelta = check.date.timestamp() - datetime.now().timestamp()
            
            if timedelta<36000:
                if check.status.name == 'active':
                    status_color = 'red'
                    url = '/checklists/%s/%s' % (check.check_list.id, check.id)
                    title = 'ПРОВЕРИТЬ ' + check.check_list.name
                elif check.status.name == 'done':
                    status_color = 'green'
                    url = '/reports/%s/' % (check.report.id)
                    title = 'ЗАВЕРШЕН ' + check.check_list.name
                elif check.status.name == 'in_work':
                    status_color = 'blue'
                    url = '/reports/%s/' % (check.report.id)
                    title = 'ПОДПИСАТЬ ' + check.check_list.name
                else:
                    status_color = 'grey'
                    url = '/checklists/%s/%s' % (check.check_list.id, check.id)
                    title = ' ' + check.check_list.name
            else:
                status_color = 'grey'
                url = ''
                title = check.check_list.name
                
            checks_as_list.append({
                "title": title,
                "start": check.date.strftime("%Y-%m-%dT%H:%M:%S"),
                "url": url,
                "backgroundColor": status_color,
                "borderColor": status_color
            })
            
    elif request.user.groups.filter(name='Manager').exists() or request.user.is_superuser  or request.user.is_staff:
        checks = Check.objects.all()
        checks_as_list = []
        
        for check in checks:
            
            if check.report:
                if check.status or check.report.status.id != check.status.id:
                    check.report.status = Status.objects.get(id=check.status.id)
                    check.report.save()
                
            timedelta = check.date.timestamp() - datetime.now().timestamp()
            
            if timedelta<36000:
                if check.status.name == 'active':
                    status_color = 'red'
                    url = ''
                    title = 'ПРОВЕРИТЬ ' + check.check_list.name
                elif check.status.name == 'done':
                    status_color = 'green'
                    url = '/reports/%s/' % (check.report.id)
                    title = 'ЗАВЕРШЕН ' + check.check_list.name
                elif check.status.name == 'in_work':
                    status_color = 'blue'
                    url = '/reports/%s/' % (check.report.id)
                    title = 'ПОДПИСАТЬ ' + check.check_list.name
                else:
                    status_color = 'grey'
                    url = '/checklists/%s/%s' % (check.check_list.id, check.id)
                    title = ' ' + check.check_list.name
            else:
                status_color = 'grey'
                url = ''
                title = check.check_list.name
                
            checks_as_list.append({
                "title": title,
                "start": check.date.strftime("%Y-%m-%dT%H:%M:%S"),
                "url": url,
                "backgroundColor": status_color,
                "borderColor": status_color
            })
    
    data = {'checks': checks, 'checks_as_list': json.dumps(checks_as_list)}
    
    return render(request, 'scheadule/scheadule.html', data)
