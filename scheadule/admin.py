from django.contrib import admin 
from .models import *
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect

class CheckAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Check._meta.fields]
    list_filter = ['check_list', 'supervisor', 'status']
    exclude = ['report', 'status']
        
    def get_form(self, request, obj=None, **kwargs):
        form = super(CheckAdmin, self).get_form(request, obj, **kwargs)
        if request.user.groups.filter(name='Supervisor').exists():
            form.base_fields['supervisor'].initial = User.objects.get(id=request.user.id)
        return form
    
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if request.user.groups.filter(name='Supervisor').exists():
            if db_field.name == "supervisor":
                kwargs["queryset"] = User.objects.filter(id=request.user.id)
        return super(CheckAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
    
    def response_add(self, request, obj, post_url_continue="../%s/"):
        if '_continue' not in request.POST:
            return HttpResponseRedirect('/scheadule/')
        else:
            return super(CheckAdmin, self).response_add(request, obj, post_url_continue)

    
    class Meta:
        model = Check
        
admin.site.register(Check, CheckAdmin)

class Check_statusAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Check_status._meta.fields]
    list_filter = ['name']
    class Meta:
        model = Check_status
        
admin.site.register(Check_status, Check_statusAdmin)