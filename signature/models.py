from django.db import models
from checklist.models import CheckList
from report.models import Report

class Position(models.Model):
    name = models.CharField(max_length=250, verbose_name='название')
    created = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name='создан')
    
    def __str__(self):
        return("%s" % (self.name))
    
    class Meta:
        verbose_name = 'Должность'
        verbose_name_plural = 'Должности'
    
class Person(models.Model):
    name = models.CharField(max_length=250)
    surname = models.CharField(max_length=250)
    patronymic = models.CharField(max_length=250)
    position = models.ForeignKey(Position, blank=True, null=True, default=None, on_delete=models.SET_NULL)
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    
    def __str__(self):
        return("%s" % (self.name))
    
    class Meta:
        verbose_name = 'Работник'
        verbose_name_plural = 'Работники'
    
class CheckListSign(models.Model):
    position = models.ForeignKey(Position, blank=True, null=True, default=None, on_delete=models.SET_NULL)
    check_list = models.ForeignKey(CheckList, blank=True, null=True, default=None, on_delete=models.SET_NULL)
    
    def __str__(self):
        return("%s" % (self.position.name))
    
    class Meta:
        verbose_name = 'Подпись'
        verbose_name_plural = 'Подписи'

class ReportSign(models.Model):
    data_image = models.TextField()
    json_image = models.TextField(default='')
    report = models.ForeignKey(Report, blank=True, null=True, default=None, on_delete=models.SET_NULL)
    person = models.ForeignKey(Person, blank=True, null=True, default=None, on_delete=models.SET_NULL)
    
    def __str__(self):
        return("%s" % (self.person.name))