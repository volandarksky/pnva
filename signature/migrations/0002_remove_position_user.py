# Generated by Django 2.2.6 on 2019-10-23 08:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('signature', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='position',
            name='user',
        ),
    ]
