from django.contrib import admin
from .models import *

class PersonAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Person._meta.fields]
    list_filter = ['position']
    class Meta:
        model = Person
        
admin.site.register(Person, PersonAdmin)

class ReportSignAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ReportSign._meta.fields]
    # list_filter = ['position']
    class Meta:
        model = ReportSign
        
# admin.site.register(ReportSign, ReportSignAdmin)

class PositionAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Position._meta.fields]
    # list_filter = ['position']
    class Meta:
        model = Position
        
admin.site.register(Position, PositionAdmin)