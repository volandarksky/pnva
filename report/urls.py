from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from . import views

admin.autodiscover()

urlpatterns = [
    url(r'^reports/$', views.reports, name='reports'),
    url(r'^reports/create_report/$', views.create_report, name='create_report'),
    url(r'^reports/add_sign/$', views.add_sign, name='add_sign'),
    url(r'^reports/copy_sign/$', views.copy_sign, name='copy_sign'),
    url(r'^reports/(?P<report_id>[0-9]+)/$', views.report, name='report'),
    url(r'^reports/(?P<report_id>[0-9]+)/pdf/$', views.report_pdf, name='report_pdf'),
]
