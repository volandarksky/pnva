from django.contrib import admin
from .models import *
from signature.models import ReportSign

class SignInline(admin.TabularInline):
    model = ReportSign
    extra = 0

class ReportFieldIline(admin.TabularInline):
    model = ReportField
    extra = 0

class ReportAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Report._meta.fields]
    inlines = (SignInline, ReportFieldIline)
    class Meta:
        model = Report
        
admin.site.register(Report, ReportAdmin)

class ReportPointAdmin(admin.ModelAdmin):
    list_display = [field.name for field in ReportPoint._meta.fields]
    list_filter = ['report']
    # inlines = (SignInline)
    class Meta:
        model = ReportPoint
        
admin.site.register(ReportPoint, ReportPointAdmin)

