from django.db import models
from django.contrib.auth.models import User
from checklist.models import CheckList, Point
from status.models import Status

class Report(models.Model):
    check_list = models.ForeignKey(CheckList, blank=True, null=True, default=None, on_delete=models.SET_NULL, verbose_name='чеклист')
    supervisor = models.ForeignKey(User, blank=True, null=True, default=None, on_delete=models.SET_NULL, verbose_name='супервизор')
    created = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name='создан')
    status = models.ForeignKey(Status, blank=True, null=True, default=Status.objects.get(name='in_work'), on_delete=models.SET_NULL, verbose_name='статус')
    # status = models.ForeignKey(Status, blank=True, null=True, on_delete=models.SET_NULL, verbose_name='статус')
    
    well_number = models.CharField(blank=True, null=True, default="", max_length=200, verbose_name='скважина')
    cluster_number =  models.CharField(blank=True, null=True, default="", max_length=200, verbose_name='куст')
    occurrence = models.CharField(blank=True, null=True, default="", max_length=200, verbose_name='месторождение')
    drilling_contractor = models.CharField(blank=True, null=True, default="", max_length=200, verbose_name='подрядчик')
    drilling_contractor = models.CharField(blank=True, null=True, default="", max_length=200, verbose_name='подрядчик')
    ongoing_work = models.CharField(blank=True, null=True, default="", max_length=200, verbose_name='работа')
       
    class Meta:
        verbose_name = 'Отчет'
        verbose_name_plural = 'Отчеты'
    
class ReportPoint(models.Model):
    report = models.ForeignKey(Report, blank=True, null=True, default=None, on_delete=models.SET_NULL)
    point = models.ForeignKey(Point, blank=True, null=True, default=None, on_delete=models.SET_NULL)
    status = models.BooleanField(blank=True, null=False, default=False)
    comment = models.TextField()
    created = models.DateTimeField(auto_now_add=True, auto_now=False)
    
class ReportField(models.Model):
    
    name = models.CharField(max_length=250, verbose_name='название')
    content = models.CharField(max_length=250, verbose_name='содержание')

    report = models.ForeignKey(Report, blank=True, null=True, default=None, on_delete=models.SET_NULL)
    created = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name='создан')
    
    class Meta:
        verbose_name = 'Поле в шапке'
        verbose_name_plural = 'Поля в шапке'