from django.shortcuts import render
from django.http import HttpResponse
from django.template.loader import get_template
from django.template import RequestContext, loader
from django.contrib.auth.decorators import login_required
from .models import Report, ReportPoint, ReportField
from checklist.models import CheckList, Point
from scheadule.models import Check, Check_status
from django.contrib.auth.models import User, Group
from signature.models import Position, ReportSign, Person
import json
from checklist.render import Render

@login_required
def reports(request):
    data = {}
    reports = {}
    print(request.GET)
    
    if request.method == 'GET':
        try:
            order_by = request.GET['order_by']
        except KeyError:
            order_by = '-created'
    else:
        order_by = '-created'
    
    if request.user.groups.filter(name='Supervisor').exists():
        reports = Report.objects.order_by('created').filter(supervisor__id=request.user.id).exclude(status__name='hidden').order_by(order_by, '-created')
    elif request.user.groups.filter(name='Manager').exists() or request.user.is_superuser  or request.user.is_staff:
        reports = Report.objects.order_by('created').all().exclude(status__name='hidden').order_by(order_by, '-created')
    
    data = {'reports': reports, 'order_by':order_by}
    
    return render(request, 'reports/reports.html', data)

@login_required
def report(request, report_id):
    data = {}
    report = {}

    if request.user.groups.filter(name='Supervisor').exists():
        report = Report.objects.filter(id=report_id).filter(supervisor__id=request.user.id)
    elif request.user.groups.filter(name='Manager').exists() or request.user.is_superuser  or request.user.is_staff:
        report = Report.objects.filter(id=report_id)
        
    if report != {}:
        
        report_recipients = list(User.objects.filter(groups__name='ReportRecipients').values('email'))
        report_recipients_emails = [i['email'] for i in report_recipients]
        
        report_points = ReportPoint.objects.filter(report__id=report_id).order_by('point__sub_check_list', 'point__header_position', 'point__position')
        header_field = ReportField.objects.filter(report__id=report_id)
        report_signs = ReportSign.objects.filter(report__id=report_id).order_by('id')
        for report_sign in report_signs:
            # report_sign.persons = Person.objects.filter(position__id=report_sign.person.position.id).exclude(name='', surname='').distinct()
            report_sign.signs_list = ReportSign.objects.filter(person__position__id=report_sign.person.position.id).exclude(person__name='', person__surname='').distinct()
        
        try:
            check = Check.objects.get(report__id=report_id)
        except Check.DoesNotExist:
            check = None
        data = {'report_points':report_points, 'header_field':header_field, 'report_signs':report_signs, 'report':report, 'check':check, 'report_recipients_emails':report_recipients_emails}
        
    return render(request, 'reports/report.html', data)

@login_required
def report_pdf(request, report_id):
    data = {}
    report = {}

    if request.user.groups.filter(name='Supervisor').exists():
        report = Report.objects.filter(id=report_id).filter(supervisor__id=request.user.id)
    elif request.user.groups.filter(name='Manager').exists() or request.user.is_superuser  or request.user.is_staff:
        report = Report.objects.filter(id=report_id)
        
    if report != {}:
        report_points = ReportPoint.objects.filter(report__id=report_id).order_by('point__sub_check_list', 'point__header_position', 'point__position')
        header_field = ReportField.objects.filter(report__id=report_id)
        report_signs = ReportSign.objects.filter(report__id=report_id)
        data = {'report_points':report_points, 'header_field': header_field, 'report_signs':report_signs, 'report':report}
    
    return Render.render('reports/report_pdf.html', data, report_id)
    return render(request, 'reports/report_pdf.html', data)

@login_required
def create_report(request):
    data = json.loads(request.POST['data'])
    check_list = CheckList.objects.get(id=data['check_list_id'])
    supervisor = User.objects.get(id=request.user.id)
    
    new_report = Report.objects.create(
        check_list = check_list, 
        supervisor = supervisor,
        well_number = data['header']['well_number'],
        cluster_number = data['header']['cluster_number'],
        occurrence = data['header']['occurrence'],
        drilling_contractor = data['header']['drilling_contractor'],
        ongoing_work = data['header']['ongoing_work']
        )

    #привязать отчет к чеклисту в графике
    if data['check_id']:
        check = Check.objects.get(id=data['check_id'])
        check.report = new_report
        check.status = Check_status.objects.get(name='in_work')
        check.save()

    for i in data['header_filed']:
        ReportField.objects.create(
            report = new_report,
            name = data['header_filed'][i]['name'],
            content = data['header_filed'][i]['content']
            )
        
    for i in data['points']:
        print(data['points'][i]['point_id'])
        point = Point.objects.get(id=data['points'][i]['point_id'])
        print(point.text)
        ReportPoint.objects.create(
            report= new_report,
            point= point,
            status = data['points'][i]['status'],
            comment = data['points'][i]['comment']
            )

    for i in data['positions']:
        print(data['positions'][i]['position_id'])
        position = Position.objects.get(id=data['positions'][i]['position_id'])
        person = Person.objects.create(
            name = '',
            surname = '',
            patronymic = '',
            position = position
            )
        ReportSign.objects.create(
            report = new_report,
            person = person,
            data_image = ''
            )

    return HttpResponse(new_report.id)

def add_sign(request):
    data = json.loads(request.POST['data'])
    
    position = Position.objects.get(id=data['position_id'])
    person = Person.objects.get(id=data['person_id'])
    
    person.name = data['name']
    person.surname = data['surname']
    person.patronymic = data['patronymic']
    person.position = position
    person.save()
    
    sign = ReportSign.objects.get(id=data['sign_id'])
    sign.data_image = data['data_image']
    sign.json_image = data['json_image']
    sign.person = person
    sign.save()
    
    return HttpResponse(request)

def copy_sign(request):
    data = json.loads(request.POST['data'])
    
    new_sign = ReportSign.objects.get(id=data['new_sign_id'])
    report_sign = ReportSign.objects.filter(id=data['sign_id'])

    report_sign.update(
        data_image = new_sign.data_image,
        json_image = new_sign.json_image,
        person = new_sign.person
    )
    
    return HttpResponse(request)
