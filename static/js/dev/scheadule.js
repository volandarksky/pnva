document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var checks = $('#checks_as_list').data('json')

    var calendar = new FullCalendar.Calendar(calendarEl, {
        height: "auto",
        plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'list', 'bootstrap' ],
        themeSystem: 'litera',
        defaultView: 'listMonth',
        locale: 'ru',
        buttonText: {
            today:    'сегодня',
            month:    'месяц',
            week:     'неделя',
            day:      'день',
            list:     'список'
            },
        header: {
            left: 'today prev,next ',
            center: 'title',
            right: 'dayGridMonth, listMonth'
        },
        events: checks
    });

    calendar.render();
});