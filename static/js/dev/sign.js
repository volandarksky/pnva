function check_signs(onload=true) {
    var check_res = false
    $('.sign_element').each(function(i) {
        var check = true;
        var person_position = $(this).find('.person_position').text();
        var err_msg = "";

        $(this).find('.sign_text').each(function(i) {
            if ($(this).text()=='') {
                err_msg = person_position+' Не все данные ФИО заполнены'
                check = false;
                check_res = false;
            }
        });

        if ($(this).find('.sign_img').attr('src') == "") {
            err_msg = person_position+' Нет подписи';
            check = false;
            check_res = false;
        }

        if (check == false) {
            if (onload==false) {
                $('#sign_err_msg').text(err_msg)
                $('#show_sign_err').modal('show')
            }
            $('.finish_report').removeClass('btn-success')
            $('.finish_report').addClass('btn-primary disabled')
            return false;
        }
        else{
            if (onload==false) {
                check_res = true
            }
            return true;
        }
    });
    return check_res
}

$(window).on('load', function() {
    if ($("div").is(".send_report")) {        
        check_signs();
    }
    $('.sign_element').each(function(i) {

        if ($(this).find('.sign_img').attr('src') != "") {
            $(this).find('.sign_btn').text('Изменить')
            $(this).find('.sign_btn').removeClass('btn-danger')
            $(this).find('.sign_btn').addClass('btn-secondary')
        }
        else{
            var a = true;
            $(this).find('.sign_text').each(function(i) {
                if ($(this).text()!='') {
                    a = false;
                    return false;
                }
            });
            if (a === false) {
                $(this).find('.sign_btn').text('Изменить')
                $(this).find('.sign_btn').removeClass('btn-danger')
                $(this).find('.sign_btn').addClass('btn-secondary')
            }
        }
    });
});

$('.finish_report').click(function(){
    check_res = check_signs(false);
    $('.finish_report').addClass('d-none');
    $('.spinner-border.text-success').removeClass('d-none');

    if (check_res==true) {
        report_id = $('#report_id').data('id')
        var request = $.ajax({
            url: '/send_report_mail/'+report_id,
            type: 'GET',
            success: function(d){
                $('.spinner-border.text-success').addClass('d-none');
                alert('Письмо отправлено');
                setTimeout(location.reload(true), 3000);
            }
        })
        request.done(function(data) {
        });
        request.fail(function(jqXHR, textStatus) {
        }); 
    }
});

$('.open_modal').click(function(){
    person_id = $(this).data('person_id')

    $('.surname').change(function() {
        fio = $(this).val().split(/\s+/)
        $('.surname').val(fio[0][0].toUpperCase()+fio[0].slice(1))
        $('.name').val(fio[1][0].toUpperCase()+fio[1].slice(1))
        $('.patronymic').val(fio[2][0].toUpperCase()+fio[2].slice(1))
    });
    
    clear_click_count = 0
    click_canvas_count = 0

    $('.sketchpad_'+person_id+'_clear').hide()

    var sketchpad = new Sketchpad({
        element: '#sketchpad_'+person_id,
        width: 1000,
        height: 400,
        color: '#005fc5',
        penSize: 2
    });

    $('#sketchpad_'+person_id).mousedown(function(){
        $('.sketchpad_'+person_id+'_clear').show();
        click_canvas_count += 1;
    });
    $('#sketchpad_'+person_id).bind("touchstart", function(){
        $('.sketchpad_'+person_id+'_clear').show();
        click_canvas_count += 1;
    });

    $('.exampleModalLabel_'+person_id).text($('.person_'+person_id+'_position').text())
    name = $('.person_'+person_id+'_name').text()
    surname = $('.person_'+person_id+'_surname').text()
    patronymic = $('.person_'+person_id+'_patronymic').text()

    $('.sign_modal_'+person_id).find('.name').val(name)
    $('.sign_modal_'+person_id).find('.surname').val(surname)
    $('.sign_modal_'+person_id).find('.patronymic').val(patronymic)

    $('.sketchpad_'+person_id+'_clear').click(function(){
        sketchpad.undo()
        clear_click_count += 1

        if (clear_click_count == clear_canvas_count) {
            $('.sketchpad_'+person_id+'_clear').hide()
        }
    })

    $('.add_sign_'+person_id).click(function(){
        
        sign_data = {}

        sign_data['person_id'] = person_id
        sign_data['sign_id'] = $('.sign_id_person_'+person_id).data('sign_id')
        sign_data['position_id'] = $('.position_person_'+person_id).data('position_id') 

        sign_data['name'] = $('.sign_modal_'+person_id).find('.name').val();
        sign_data['surname'] = $('.sign_modal_'+person_id).find('.surname').val();
        sign_data['patronymic'] = $('.sign_modal_'+person_id).find('.patronymic').val();
    
        canvas = document.getElementById('sketchpad_'+person_id);
        sign_data['json_image'] = sketchpad.toJSON()
        if (JSON.parse(sign_data['json_image']).strokes.length==0) {
            sign_data['data_image'] = ""
        }
        else{
            sign_data['data_image'] = canvas.toDataURL();
        }

        $('.person_'+person_id+'_name').text(sign_data['name'])
        $('.person_'+person_id+'_surname').text(sign_data['surname'])
        $('.person_'+person_id+'_patronymic').text(sign_data['patronymic'])

        $(".sign_"+person_id).attr("src", sign_data['data_image']);
        $('.sign_modal_'+person_id).find('form')[0].reset();

        send_data = {
            data: sign_data,
            csrfmiddlewaretoken: getCookie('csrftoken')
        }
        send_data['data'] = JSON.stringify(send_data['data'])

        var request = $.ajax({
            url: '/reports/add_sign/',
            type: 'POST',
            data: send_data,
            success: function(d){
                setTimeout(function() {window.location.reload();}, 3000);
            }
        })
        request.done(function(data) {
        });
        request.fail(function(jqXHR, textStatus) {
        });
    })
})


$('.copy_sign').click(function(){
    sign_data = {
        report_id: '',
        sign_id: '',
    }
    sign_data['report_id'] = $(this).data('report_id')
    sign_data['sign_id'] = $(this).data('sign_id')
    sign_data['new_sign_id'] = $(this).data('new_sign_id')

    send_data = {
        data: sign_data,
        csrfmiddlewaretoken: getCookie('csrftoken') 
    }
    send_data['data'] = JSON.stringify(send_data['data'])

    var request = $.ajax({
        url: '/reports/copy_sign/',
        type: 'POST',
        data: send_data,
        success: function(d){
            setTimeout(function() {window.location.reload();}, 1500);
        }
    })
    request.done(function(data) {
    });
    request.fail(function(jqXHR, textStatus) {
    });  
})