$('.checkbox.checkbox-slider--b-flat').click(function(){
    checkbox_id = $(this).data('checkbox_id')
    console.log(checkbox_id);
    
    if ($(this).children('label').children('input').prop('checked')) {
        $(this).children('label').children('input').attr( 'checked', true)
        localStorage["checkbox_"+checkbox_id] = 1;
    }
    else{
        $(this).children('label').children('input').attr( 'checked', false)
        localStorage.removeItem("checkbox_"+checkbox_id);
    }
})

function validation() {
    result = true;
    $('.checklist_header_data').each(function(i) {
        if ($(this).find('.form-control').val()=="") {
            $('#checklist_err_msg').text('Заполните поле: '+$(this).find('.form-check-label').text())
            $('#show_checklist_err').modal('show')
            result = false;
            return false;
        }
    });
    return result;
}

function send_checklist() {
    send_data = {
        data: {
            header: {},
            header_filed: {},
            points: {},
            positions: {},
            check_list_id: $('.checklist_id').data('checklist_id'),
            check_id: $('.check_id').data('check_id'),
        },
        csrfmiddlewaretoken: getCookie('csrftoken')
    }

    send_data['data']['header']['well_number'] = $('#well_number').val()
    send_data['data']['header']['cluster_number'] = $('#cluster_number').val()
    send_data['data']['header']['occurrence'] = $('#occurrence').val()
    send_data['data']['header']['drilling_contractor'] = $('#drilling_contractor').val()
    send_data['data']['header']['ongoing_work'] = $('#ongoing_work').val()
   
    $('.header_field').each(function(i) {

        header_field_data = {
            name: $(this).data('name'),
            content: $(this).val()
        }
        send_data['data']['header_filed'][i] = header_field_data
    });

    $( ".point_data" ).each(function( index ) {
        point_data = {
            point_id: $(this).find('.checkbox_ios').data('point_id'),
            status: $(this).find('.checkbox_ios').prop('checked'),
            comment: $(this).find('.form-control.comment').val()
        }
        send_data['data']['points'][index] = point_data
    });

    $( ".position_id" ).each(function( index ) {
        positions_data = {
            position_id: $(this).data('position_id'),
        }
        console.log(positions_data);
        
        send_data['data']['positions'][index] = positions_data
    });

    send_data['data'] = JSON.stringify(send_data['data'])

    var request = $.ajax({
        url: '/reports/create_report/',
        type: 'POST',
        data: send_data,
        success: function(d){
            url = '/reports/'+d
            window.location.replace(url)
        }
    });
    
    request.done(function(data) {
    });
    
    request.fail(function(jqXHR, textStatus) {
    });
}

$('#send_checklist').click(function(){
    if (validation()==true) {
        send_checklist();
    }
})

$('#skip_checklist').click(function(){
    localStorage.clear();
    document.location.reload(true);
})

$(window).on('load', function() {
    for(let key in localStorage) {
        if (key.indexOf('checkbox_') >= 0) {
            id = key.split('_')[1]
            $('#checkbox_ios_'+id).attr( 'checked', 'checked' )
        }
        else if (key.indexOf('comment_') >= 0) {
            id = key.split('_')[1]
            $('.comment_'+id).val(localStorage.getItem(key));
        }
    }
});

$('.comment').change(function() {
    id = $(this).data('id');
    text = $(this).val();
    localStorage["comment_"+id] = text;
});

$('.show_schedule_pdf').click(function(){
    show_schedule()
})

$('.hidden_schedule_pdf_bottom').click(function(){
    show_schedule()
})

function show_schedule() {

    if ($('.schedule_pdf').hasClass('d-none')) {

        $('.schedule_pdf').removeClass('d-none');
        $('.schedule_pdf').addClass('d-block');

        $('.show_schedule_pdf').text('скрыть график');

        $('.hidden_schedule_pdf_bottom').removeClass('d-none');
        $('.hidden_schedule_pdf_bottom').addClass('d-block');
    }
    else{

        $('.schedule_pdf').removeClass('d-block');
        $('.schedule_pdf').addClass('d-none');

        $('.show_schedule_pdf').text('показать график');

        $('.hidden_schedule_pdf_bottom').removeClass('d-block');
        $('.hidden_schedule_pdf_bottom').addClass('d-none');
    }
}

$('.comment').click(function(){
    id = $(this).data('id')
    comment = $(this).val()
    $('#exampleFormControlTextarea_'+id).val(comment)
})

$('.btn-comment').click(function(){
    id = $(this).data('id')
    comment = $('#exampleFormControlTextarea_'+id).val()
    $('.comment_'+id).val(comment)
    $('#exampleModalComment_'+id).modal('hide');
})