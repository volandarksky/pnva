$('.show_report_recipients_emails').click(function(){
    show_recipients_emails()
})

$('.hidden_report_recipients_emails_bottom').click(function(){
    show_recipients_emails()
})

function show_recipients_emails() {

    if ($('.report_recipients_emails').hasClass('d-none')) {

        $('.report_recipients_emails').removeClass('d-none');
        $('.report_recipients_emails').addClass('d-block');

        $('.show_report_recipients_emails').text('скрыть список рассылки');

        $('.hidden_report_recipients_emails_bottom').removeClass('d-none');
        $('.hidden_report_recipients_emails_bottom').addClass('d-block');
    }
    else{

        $('.report_recipients_emails').removeClass('d-block');
        $('.report_recipients_emails').addClass('d-none');

        $('.show_report_recipients_emails').text('показать список рассылки');

        $('.hidden_report_recipients_emails_bottom').removeClass('d-block');
        $('.hidden_report_recipients_emails_bottom').addClass('d-none');
    }
} 